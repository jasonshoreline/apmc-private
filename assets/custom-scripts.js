(function($) {
	
    //TAB FUNCTIONS
    $(document).ready(function($) {

        $('.tab-menu a:first-of-type').addClass( 'active' );

        $('.tab-menu a').on('click', function(e) {
            e.preventDefault;
            $('.tab-menu a').not(this).removeClass( 'active' );
            $(this).addClass( 'active' );
            var target = $(this).data( 'name' );

            var clean = target.replace( " ", ""  );
            var section = $( '#' + clean  ); 
            $('.tab-content').not( section ).fadeOut( '' );
            $( section  ).css({ 'opacity' : '1' }).fadeIn( '' );


        });
    });
	
})( jQuery );