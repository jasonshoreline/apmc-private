Vagrant.configure(2) do |config|
  vagrant_version = Vagrant::VERSION.sub(/^v/, '')
  config.env.enable
  config.vm.box = "ubuntu/trusty64"

  config.vm.provision "fix-no-tty", type: "shell" do |s|
    s.privileged = false
    s.binary = true
    s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  # Shopify FTP Proxy
  config.vm.network "forwarded_port", guest: 2121, host: 2121
  config.vm.network "forwarded_port", guest: 39031, host: 39031
  config.vm.network "forwarded_port", guest: 53, host: 53

  # Disable default synced folder
  config.vm.synced_folder ".", "/vagrant", disabled: true

  if vagrant_version >= "1.3.0"
    config.vm.synced_folder "./", "/vagrant", create: true, :owner => "vagrant", :mount_options => [ "dmode=775", "fmode=774" ]
  else
    config.vm.synced_folder "./", "/vagrant", create: true, :owner => "vagrant", :extra => 'dmode=775,fmode=774'
  end

  config.ssh.forward_agent = true

  config.vm.provision :shell, path: ".provision/provision.sh", privileged: false, binary: true
  config.vm.provision :shell, path: ".provision/install-rvm.sh", args: "stable", privileged: false
  config.vm.provision :shell, path: ".provision/install-ruby.sh", args: "1.9.3", privileged: false
  config.vm.provision :shell, path: ".provision/install-ruby.sh", args: "2.2.2 rails bundler", privileged: false
  config.vm.provision :shell, path: ".provision/install-tools.sh", privileged: false
  config.vm.provision :shell, path: ".provision/project.sh", privileged: false, binary: true


# Add your host machine's SSH config and main SSH key to your vagrant user folder
config.vm.provision :file, source: "~/.ssh/config", destination: "/home/vagrant/.ssh/config"
config.vm.provision :file, source: "~/.ssh/id_rsa", destination: "/home/vagrant/.ssh/id_rsa"
config.vm.provision :file, source: "~/.ssh/id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub"

end
